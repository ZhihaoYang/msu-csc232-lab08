//Student Generate Code Assignment
//Zhihao Yang, Samuel Young

#include <iostream>
#include "binary_search.cpp"
#include "selection_sort.cpp"
using namespace std;

int findMean(int Array[], int arraySize){
  int sum = 0;
  for (int i=0; i <= arraySize; i++){
    sum = sum + Array[i];
  }
  return sum / arraySize;
}

int main(){
  int MAX_ARRAY_SIZE = 50;
  int mainArraySize;
  cout << "Enter the desired size of the array: ";
  cin >> mainArraySize;
  int mainArray[mainArraySize];
  for(int count = 0; count <= mainArraySize; count++){
    cout << "Please enter an integer for position " << count+1 << " ";
    cin >> mainArray[count];
  }
  displayArray(mainArray,mainArraySize);
  selectionSortArray(mainArray,mainArraySize);
  displayArray(mainArray,mainArraySize);
  int num2find;
  cout << "Enter a number to search for:" << endl;
  cin >> num2find;
  int found;
  found = binarySearch(mainArray, mainArraySize, num2find);
  if (found == -1)
		cout << "The value " << num2find << " is not in the list" << endl;
	else
		cout << "The value " << num2find << " is in position number "
		     << found + 1 << " of the list" << endl;
  int mean;
  mean = findMean(mainArray,mainArraySize);
  cout << "The array's mean is " << mean;

  return 0;
}